<?php

namespace Drupal\restrict_ip_email_domains\Service;

use Drupal\restrict_ip\Service\RestrictIpService;

/**
 * Decorates the RestrictIpService service.
 *
 * @package Drupal\restrict_ip_email_domains\Service
 */
class RestrictIpEdServiceDecorator extends RestrictIpService {

  /**
   * {@inheritdoc}
   */
  public function userIsBlocked() {
    if (!$this->restrictedEmailDomain()) {
      return FALSE;
    }

    return parent::userIsBlocked();
  }

  /**
   * {@inheritdoc}
   */
  public function testForBlock($runInCli = FALSE) {
    if (!$this->restrictedEmailDomain()) {
      $this->blocked = FALSE;
    }
    else {
      parent::testForBlock($runInCli);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getRestrictedEmailDomains() {
    $emailDomains = $this->mapper->getRestrictedEmailDomains();

    return is_array($emailDomains) ? $emailDomains : [];
  }

  /**
   * {@inheritdoc}
   */
  public function saveRestrictedEmailDomains(array $emailDomains, $overwriteExisting = TRUE) {
    $this->mapper->saveRestrictedEmailDomains($emailDomains, $overwriteExisting);
  }

  /**
   * Checks if current user email has a restricted domain.
   */
  public function restrictedEmailDomain() {
    $currentUserEmail = $this->currentUser->getEmail();
    $emailDomains = $this->getRestrictedEmailDomains();

    foreach ($emailDomains as $emailDomain) {
      if (strpos($currentUserEmail, $emailDomain) !== FALSE) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
