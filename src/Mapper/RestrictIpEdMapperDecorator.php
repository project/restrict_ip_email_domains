<?php

namespace Drupal\restrict_ip_email_domains\Mapper;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\restrict_ip\Mapper\RestrictIpMapper;
use Drupal\Core\Database\Connection;

/**
 * Decorates the RestrictIpMapper service.
 *
 * @package Drupal\restrict_ip_email_domains\Mapper
 */
class RestrictIpEdMapperDecorator extends RestrictIpMapper {

  /**
   * Stores the configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  public function __construct(ConfigFactoryInterface $config_factory, Connection $connection) {
    $this->configFactory = $config_factory;
    parent::__construct($connection);
  }

  /**
   * {@inheritdoc}
   */
  public function getRestrictedEmailDomains() {
    $config = $this->configFactory->get('restrict_ip_email_domains.settings');
    return $config->get('email_domain');
  }

  /**
   * {@inheritdoc}
   */
  public function saveRestrictedEmailDomains(array $email_domains) {
    $config = $this->configFactory->getEditable('restrict_ip_email_domains.settings');
    $emails = [];
    foreach ($email_domains as $email_domain) {
      $emails[] = trim($email_domain);
    }
    $config->set('email_domain', $emails)->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getWhitelistedIpAddresses()
  {
    $config = $this->configFactory->get('restrict_ip_email_domains.settings');
    return $config->get('ip_addresses');
  }

  /**
   * {@inheritdoc}
   */
  public function saveWhitelistedIpAddresses(array $ip_addresses, $overwriteExisting = TRUE)
  {
    $config = $this->configFactory->getEditable('restrict_ip_email_domains.settings');
    $addresses = [];
    foreach ($ip_addresses as $ip_address) {
      $addresses[] = trim($ip_address);
    }
    $config->set('ip_addresses', $addresses)->save();
  }


  /**
   * {@inheritdoc}
   */
  public function getWhitelistedPaths()
  {
    $config = $this->configFactory->get('restrict_ip_email_domains.settings');
    return $config->get('whitelist_path');
  }

  /**
   * {@inheritdoc}
   */
  public function saveWhitelistedPaths(array $whitelistedPaths, $overwriteExisting = TRUE)
  {
    $config = $this->configFactory->getEditable('restrict_ip_email_domains.settings');
    $paths = [];
    foreach ($whitelistedPaths as $whitelistedPath) {
      $paths[] = trim($whitelistedPath);
    }
    $config->set('whitelist_path', $paths)->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getBlacklistedPaths()
  {
    $config = $this->configFactory->get('restrict_ip_email_domains.settings');
    return $config->get('blacklist_path');
  }

  /**
   * {@inheritdoc}
   */
  public function saveBlacklistedPaths(array $blacklistedPaths, $overwriteExisting = TRUE)
  {
    $config = $this->configFactory->getEditable('restrict_ip_email_domains.settings');
    $paths = [];
    foreach ($blacklistedPaths as $blacklistedPath) {
      $paths[] = trim($blacklistedPath);
    }
    $config->set('blacklist_path', $paths)->save();
  }

}
