**THIS MODULE HAS BEEN CONTRIBUTED TO DRUPAL.ORG**
**PLEASE USE THE [CONTRIBUTED VERSION](https://www.drupal.org/project/restrict_ip_email_domains)**
**USE THIS REPOSITORY FOR ANY DEVELOPMENT AND QA THEN SYNC IT TO DRUPAL.ORG WITH A RAISED PR**


CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

Restrict IP Email Domain extends the Restrict IP module, adding the 
functionality of applying the restrictions only to 
a specific set of email domains.
Installing this module will limit the Restrict IP settings only to 
that set of specific email domains.


REQUIREMENTS
------------

This module requires the following modules:

  * Restrict IP (https://www.drupal.org/project/restrict_ip) (>= 3.0)
  

INSTALLATION
------------

  * Install as you would normally install a contributed Drupal module. 
  Visit: https://www.drupal.org/docs/extending-drupal/installing-modules
  for further information.


CONFIGURATION
-------------

  * Visit the Restrict IP configuration page (/admin/config/people/restrict_ip)
  * Add your set of email domains in the Restricted email domains field
